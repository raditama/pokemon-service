package com.pokemon.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pokemon")
public class PokemonController {

	@GetMapping("/probability")
	public Map<String, Object> getProbability() {
		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			boolean result = false;

			double random = Math.random() * 2;
			if ((int)random == 1) {
				result = true;
			}
			
			response.put("message", "Success!");
			response.put("response", result);
			response.put("status", 200);
		} catch (Exception e) {
			response.put("message", "Failed!");
			response.put("response", e);
			response.put("status", 500);
		}
		
		return response;
	}
	
	@GetMapping("/release")
	public Map<String, Object> getRelease() {
		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			boolean result = false;

			double random = Math.random() * 10;
			
			int n = (int)random;
			int m = n/2;
			
			if(n==0||n==1) {
				result = false;
			} else {
				for(int i = 2; i <= m; i++) {
					if(n%i==0) {
				    	result = false;
				    	break;
				    }
					result = true;
				}      
				
			}
			
			response.put("message", "Success!");
			response.put("response", result);
			response.put("number", n);
			response.put("status", 200);
		} catch (Exception e) {
			response.put("message", "Failed!");
			response.put("response", e);
			response.put("status", 500);
		}
		
		return response;
	}
	
	@GetMapping("/rename/{mount}/{nickname}")
	public Map<String, Object> getRename (@PathVariable(value = "mount") int mount, @PathVariable(value = "nickname") String nickname) {
		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			String result;
			
			if (mount == 0) {
				result = nickname + "-" + fibonaccy(mount);
			} else {
				result = nickname.substring(0, nickname.length() - 1) + fibonaccy(mount);
			}
			
			response.put("message", "Success!");
			response.put("response", result);
			response.put("current", nickname);
			response.put("status", 200);
		} catch (Exception e) {
			response.put("message", "Failed!");
			response.put("response", e);
			response.put("status", 500);
		}
		return response;
	}
	
	static int fibonaccy(int n) {
	    if (n <= 1) {
	       return n;
	    }
	    return fibonaccy(n-1) + fibonaccy(n-2);
    }
}
